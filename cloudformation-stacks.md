#To run cloudformation, we have to write up templates that run from a JSON file. 

However, many of the templates would be saved in .yml files, do find a number of files saved as [n]stack.yml, where n is a corresponding number. 

#To kick off Cloudformation, run the code below. 

aws cloudformation create-stack --stack-name blog-stage --template-body file://$PWD/1stack.yml --profile commy --region us-east-1

# Should get a desired outcome, like .. 

{
    "StackId": "arn:aws:cloudformation:us-east-2:239636875734:stack/blog-stage/b386cf40-3d98-11e8-accb-50faf8c48cf2"
}

# when our stack in cloudformation seems to be up and running, we can now describe our stack with the following command. 

aws cloudformation describe-stack-resources --stack-name blog-stage --profile commy

# and the output should be .. 


{
    "StackResources": [
        {
            "StackId": "arn:aws:cloudformation:us-east-2:239636875734:stack/blog-stage/b386cf40-3d98-11e8-accb-50faf8c48cf2", 
            "ResourceStatus": "DELETE_COMPLETE", 
            "ResourceType": "AWS::EC2::Instance", 
            "Timestamp": "2018-04-11T14:58:03.102Z", 
            "StackName": "blog-stage", 
            "LogicalResourceId": "AppNode"
        }, 
        {
            "StackId": "arn:aws:cloudformation:us-east-2:239636875734:stack/blog-stage/b386cf40-3d98-11e8-accb-50faf8c48cf2", 
            "ResourceStatus": "DELETE_COMPLETE", 
            "ResourceType": "AWS::EC2::SecurityGroup", 
            "Timestamp": "2018-04-11T14:58:04.857Z", 
            "StackName": "blog-stage", 
            "PhysicalResourceId": "blog-stage-AppNodeSG-SJQXQG7ZH0SU", 
            "LogicalResourceId": "AppNodeSG"
        }
    ]
}

#Now that I have tested the first cloudformation Stack, I can delete, and attempt installing docker.

to delete the stack, blog-stage, I would execute the folowing code below. 

aws cloudformation delete-stack --stack-name blog-stage --profile commy


#Run the second (2stack.yml) file to provision an EC2 instance with docker engine preistalled .

aws cloudformation create-stack --stack-name blog-stage2 --template-body file://$PWD/2stack.yml --profile commy --region us-east-1


