#Create a keypair to access instances

Key Pair : comcast-key-1.pem

with the ".pem" key pair, you should be able to access any Ec2 instances or whatever instance that requires an identity file to access its console via ssh. 



##Steps 

1. Find and Click EC2 instances

2. while in EC2 instances, click Key Pairs under the Network Security settings tab on the left hand navigation. 

3. Create a new Key Pair, Click Create new Key Pair

4. Enter a new key pair name : comcast-key-1

5. Click create, a pem file "comcast-key-1.pem" would automatically be downloaded 


#NOTE 

remember to change the permision of the pair key appropraitely before intended use. 




 


